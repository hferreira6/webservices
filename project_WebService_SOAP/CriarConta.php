<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Criar Conta</title>
	</head>
	<body>
		<form method="get" action="CriarConta.php">
			<big><strong>Criar Conta</strong></big>
			<br />
			<strong>Nome:</strong>
			<input name="nome" type="text" size="10" maxlength="200" value="" />
			<br />
			<strong>Apelido:</strong>
			<input name="apelido" type="text" size="10" maxlength="200" value="" />
			<br />
			<strong>Data de Nascimento:</strong>
			<input name="datanasc" type="text" size="10" maxlength="200" value="" />
			<br />
			<strong>* E-Mail:</strong>
			<input name="email" type="text" size="50" maxlength="200" value="" />
			<br />
			<strong>Morada:</strong>
			<input name="morada" type="text" size="10" maxlength="200" value="" />
			<br />
			<strong>Sexo:</strong>
			<input name="sexo" type="text" size="1" maxlength="5" value="" />
			<br />
			<strong>* Utilizador:</strong>
			<input name="user" type="text" size="10" maxlength="200" value="" />
			<br />
			<strong>* Palavra Chave:</strong>
			<input name="pass" type="password" size="10" maxlength="200" value="" />
			<br />
			(*) - Campos de preenchimento obrigatório.
			<input type="submit" value="ok" />
			<br />
		</form>
		<?php
		// Pull in the NuSOAP code
		require_once "../lib/nusoap.php";
		// Create the client instance
		$client = new nusoap_client("http://localhost/Prog/Php/project/WS.php");
		// Check for an error
		$err = $client -> getError();
		if ($err) {
			// Display the error
			echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
			// At this point, you know the call that follows will fail
		}
		if (!empty($_GET["user"]) || !empty($_GET["pass"]) || !empty($_GET["email"])) {
			// Call the SOAP method - don't call both at the same time !
			$result = $client -> call('criarConta', array('nome' => $_GET["nome"], 'apelido' => $_GET["apelido"], 'datanasc' => $_GET["datanasc"], 'email' => $_GET["email"], 'morada' => $_GET["morada"], 'sexo' => $_GET["sexo"], 'user' => $_GET["user"], 'pass' => $_GET["pass"]));
		}
	?>
<?php
if (!empty($result)) {
	// Check for a fault
	if ($client -> fault) {
		echo '<h2>Fault</h2><pre>';
		print_r($result);
		echo '</pre>';
	} else {
		// Check for errors
		$err = $client -> getError();
		if ($err) {
			// Display the error
			echo '<h2>Error</h2><pre>' . $err . '</pre>';
		} else {
			// Display the result
			if ($result) {
				echo '<h2>Conta criada com sucesso.</h2>';
				header('Location: http://localhost/ES/Project/PhpProjectES/login.php');
			} else {
				echo '<h2>Erro na criação da conta!</h2>';
			}
		}
	}
	// Display the request and response
	echo '<h2>Request</h2>';
	echo '<pre>' . htmlspecialchars($client -> request, ENT_QUOTES) . '</pre>';
	echo '<h2>Response</h2>';
	echo '<pre>' . htmlspecialchars($client -> response, ENT_QUOTES) . '</pre>';
	// Display the debug messages
	echo '<h2>Debug</h2>';
	echo '<pre>' . htmlspecialchars($client -> debug_str, ENT_QUOTES) . '</pre>';
}
		?>
	</body>
</html>