<?php
// To use with the getTime function
date_default_timezone_set('UTC');
// Pull in the NuSOAP code
require_once "/home/hferreira/ufp/Prog/Php/lib/nusoap.php";
// Change this to your namespace
$namespace = "http://localhost/Prog/Php/project/WS.php";
// Create the server instance
$server = new nusoap_server();
// Register the method to expose without WSDL
/*$server -> register('methode');*/
// Register the method to expose with WSDL
// Initialize WSDL support
$server -> configureWSDL('ssdieiwsdl', 'urn:ssdieiwsdl');
// Define our namespace
$server -> wsdl -> schemaTargetNamespace = $namespace;
$server -> register('criarConta', // method name
array('nome' => 'xsd:string', 'apelido' => 'xsd:string', 'datanasc' => 'xsd:string', 'email' => 'xsd:string', 'morada' => 'xsd:string', 'sexo' => 'xsd:string', 'user' => 'xsd:string', 'pass' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#criarConta', // soapaction
'rpc', // style
'encoded', // use
'Cria uma conta e guarda os dados em base de dados' // documentation
);
$server -> register('login', // method name
array('user' => 'xsd:string', 'pass' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#login', // soapaction
'rpc', // style
'encoded', // use
'Funcao que premite efectuar o login' // documentation
);
$server -> register('consultaInt', // method name
array('sistema' => 'xsd:string', 'firstdaterange' => 'xsd:string', 'seconddaterange' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#consultaInt', // soapaction
'rpc', // style
'encoded', // use
'Apresenta as interferencias de um sistema Cots' // documentation
);
$server -> register('removeConta', // method name
array('email' => 'xsd:string', 'pass' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#removeConta', // soapaction
'rpc', // style
'encoded', // use
'Remove a conta' // documentation
);
$server -> register('insereEstado', // method name
array('element' => 'xsd:string', 'feature' => 'xsd:string', 'value' => 'xsd:string', 'type' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#insereEstado', // soapaction
'rpc', // style
'encoded', // use
'Insere o estado de um sistema Cots' // documentation
);
$server -> register('consultaEstado', // method name
array('xbox' => 'xsd:string', 'tv' => 'xsd:string', 'phone' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#consultaEstado', // soapaction
'rpc', // style
'encoded', // use
'Consulta o estado dos sistemas Cots' // documentation
);
// Define the method as a PHP function
/**
 * Função que insere o estado de um sistema Cots
 * @param[in] element The id of the system.
 * @param[in] feature The id of the feature that system.
 * @param[in] value The state of the feature in that system.
 * @param[in] type The IN / OUT feature of that system.
 * @param[out] result The result of query - True if sucess or False is not.
 * @return True if sucess or False if insucess.
 */
function insereEstado($element, $feature, $value, $type) {
	$con = mysql_connect("127.0.0.1", "root", "");
	if ($con == 0) {
		echo 'Failed to connect to MySQL' . "<br />";
	}
	mysql_select_db("interferencestudent", $con);
	$result = mysql_query("INSERT INTO state VALUES (" . (int)$element . ", " . (int)$feature . ", \"" . $value . "\", CURRENT_TIMESTAMP, " . (int)$element . ", \"" . $type . "\")");
	mysql_close($con);
	return $result;
	/*!$result para insert é booleano (True se sucesso / False se insucesso)*/
}

/**
 * Função que consulta o estado dos sistemas Cots
 * @param[in] xbox The id of system xbox.
 * @param[in] tv The id of system television.
 * @param[in] phone The id of system phone.
 * @param[out] result The result of query.
 * @return Table of contents of sistems xbox, tv and phone.
 */
function consultaEstado($xbox, $tv, $phone) {
	$con = mysql_connect("127.0.0.1", "root", "");
	if ($con == 0) {
		echo 'Failed to connect to MySQL' . "<br />";
	}
	mysql_select_db("interferencestudent", $con);
	$result = mysql_query("SELECT * FROM state WHERE fk_element=" . (int)$xbox . " OR fk_element=" . (int)$tv . " OR fk_element=" . (int)$phone . " ORDER BY timestamp desc");
	mysql_close($con);
	while ($row = mysql_fetch_array($result)) {
		$items[] = array('ID_SYSTEM' => $row['fk_element'], 'ID_FEATURE' => $row['fk_feature'], 'VALUE' => $row['value'], 'DATE' => $row['timestamp'], 'ID_SOURCE' => $row['source'], 'INOUT' => $row['inout']);
	}
	return json_encode(array('estados' => $items));
}

/**
 * Função que cria uma conta de utilizador inserindo numa base de dados
 * @param[in] email The e-mail of client.
 * @param[in] nome The name of client.
 * @param[in] apelido The last name of client.
 * @param[in] datanasc The date of bird of client.
 * @param[in] email The e-mail of client - This field is obrigatory.
 * @param[in] morada The address of client.
 * @param[in] sexo The sex of client (M - F).
 * @param[in] user The username of client - This field is obrigatory.
 * @param[in] pass The password of client - This field is obrigatory.
 * @param[out] result The result of the query.
 * @return True if sucess or False if insucess.
 */
function criarConta($nome, $apelido, $datanasc, $email, $morada, $sexo, $user, $pass) {
	$con = mysql_connect("127.0.0.1", "root", "");
	if ($con == 0) {
		echo 'Failed to connect to MySQL' . "<br />";
	}
	mysql_select_db("contas", $con);
	$op = mysql_query("SELECT email FROM conta WHERE email=\"" . $email . "\"");
	$row = mysql_fetch_row($op);
	if ($row[0] != $email) {
		$row = mysql_query("SELECT COUNT(id) FROM conta");
		$num = mysql_fetch_row($row);
		$numindex = ($num[0] + 1);
		$result = mysql_query("INSERT INTO conta VALUES (" . $numindex . ",
				\"" . $nome . "\",
				\"" . $apelido . "\",
				\"" . $datanasc . "\",
				\"" . $email . "\",
				\"" . $morada . "\",
				\"" . $sexo . "\",
				\"" . $user . "\",
				\"" . $pass . "\"
				)");
	} else {
		$result = FALSE;
	}
	mysql_close($con);
	return $result;
}

/**
 * Função que verifica conta de utilizador
 * @param[in] user The username of client.
 * @param[in] pass The password of client.
 * @param[out] result The result of suceful query or not suceful.
 * @return True or False.
 */
function login($user, $pass) {
	$con = mysql_connect("127.0.0.1", "root", "");
	if ($con == 0) {
		echo 'Failed to connect to MySQL' . "<br />";
	}
	mysql_select_db("contas", $con);
	$result = mysql_query("SELECT user, pass FROM conta WHERE user=\"" . $user . "\" AND pass=\"" . $pass . "\"");
	$row = mysql_fetch_row($result);
	if ($row[0] == $user && $row[1] == $pass) {
		$res = TRUE;
	} else {
		$res = FALSE;
	}
	mysql_close($con);
	return $res;
}

/**
 * Função que ira consultar a base de dados por um sistema
 * @param[in] sistema The sistem Cots to search.
 * @param[in] firstdaterange The first date to search in the interference.
 * @param[in] firstdaterange The last date to search in the interference.
 * @param[out] result The result of the query in an array encoded in json.
 * @return The result of join of tables state and fioccurred in an array encoded in json.
 */
function consultaInt($sistema, $firstdaterange, $seconddaterange) {
	$con = mysql_connect("127.0.0.1", "root", "");
	if ($con == 0) {
		echo 'Failed to connect to MySQL' . "<br />";
	}
	mysql_select_db("interferencestudent", $con);
	$result = mysql_query("SELECT possfi.source SYSTEM, possfi.sync CAUSED_BY, fio.timestamp DATE, fio.solved SOLVED FROM fioccurred fio, possiblefi possfi WHERE possfi.source=\"" . $sistema . "\" AND fio.fio=possfi.PFI_ID AND timestamp > \"" . $firstdaterange . "\" AND timestamp <= \"" . $seconddaterange . "\" GROUP BY fio.timestamp");
	mysql_close($con);
	while ($row = mysql_fetch_array($result)) {
		$items[] = array('SYSTEM' => $row['SYSTEM'], 'CAUSED_BY' => $row['CAUSED_BY'], 'DATE' => $row['DATE'], 'SOLVED' => $row['SOLVED']);
	}
	return json_encode(array('estados' => $items));
}

/**
 * Função que remove conta de utilizador
 * @param[in] email The e-mail of client.
 * @param[in] pass The password of client.
 * @param[out] result The result of the query.
 * @return True if sucess or False if insucess.
 */
function removeConta($email, $pass) {
	$con = mysql_connect("127.0.0.1", "root", "");
	if ($con == 0) {
		echo 'Failed to connect to MySQL' . "<br />";
	}
	mysql_select_db("contas", $con);
	$op = mysql_query("SELECT email FROM conta WHERE email=\"" . $email . "\" AND pass=\"" . $pass . "\"");
	if ($op) {
		$result = mysql_query("DELETE FROM conta WHERE email=\"" . $email . "\" AND pass=\"" . $pass . "\"");
	}
	mysql_close($con);
	if (empty($op)) {
		$result = FALSE;
	}
	return $result;
}

// Use the request to (try to) invoke the service -  XAMMP !!!!
if (!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');
$server -> service($HTTP_RAW_POST_DATA);
?>