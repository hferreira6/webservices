<?php
// To use with the getTime function
date_default_timezone_set('UTC');
// Pull in the NuSOAP code
require_once "../lib/nusoap.php";
// Change this to your namespace
$namespace = "http://localhost/Prog/Php/project/WS1.php";
// mudar o path
// Create the server instance
$server = new nusoap_server();

$server -> configureWSDL('ssdiei1wsdl', 'urn:ssdiei1wsdl');

$server -> wsdl -> schemaTargetNamespace = $namespace;

$server -> register('criarConta', // method name
array('nome' => 'xsd:string', 'apelido' => 'xsd:string', 'datanasc' => 'xsd:string', 'email' => 'xsd:string', 'morada' => 'xsd:string', 'sexo' => 'xsd:string', 'user' => 'xsd:string', 'pass' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#criarConta', // soapaction
'rpc', // style
'encoded', // use
'Cria uma conta e guarda os dados em base de dados' // documentation
);
$server -> register('login', // method name
array('user' => 'xsd:string', 'pass' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#login', // soapaction
'rpc', // style
'encoded', // use
'Funcao que premite efectuar o login' // documentation
);

$server -> register('removeConta', // method name
array('email' => 'xsd:string', 'pass' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#removeConta', // soapaction
'rpc', // style
'encoded', // use
'Remove a conta' // documentation
);

$server -> register('pesquisaGoES', // method name
array('nomeGrafo' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#consultaEstado', // soapaction
'rpc', // style
'encoded', // use
'Pesquisa GoEs, ficheiro graph.xml' // documentation
);

$server -> register('descarregarGoes', // method name
array('nomeGrafo' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#remove', // soapaction
'rpc', // style
'encoded', // use
'Descarega o GoEs,criando um ficheiro com o nome do nodo a tirar' // documentation
);

$server -> register('removeGoes', // method name
array('nomeGrafo' => 'xsd:string'), // input parameters
array('return' => 'xsd:string'), // output parameters
'urn:ssdieiwsdl', // namespace
'urn:ssdieiwsdl#remove', // soapaction
'rpc', // style
'encoded', // use
'Remove o GoEs' // documentation
);

/**
 * Função que cria uma conta de utilizador inserindo numa base de dados
 * @param[in] email The e-mail of client.
 * @param[in] nome The name of client.
 * @param[in] apelido The last name of client.
 * @param[in] datanasc The date of bird of client.
 * @param[in] email The e-mail of client - This field is obrigatory.
 * @param[in] morada The address of client.
 * @param[in] sexo The sex of client (M - F).
 * @param[in] user The username of client - This field is obrigatory.
 * @param[in] pass The password of client - This field is obrigatory.
 * @param[out] result The result of the query.
 * @return True if sucess or False if insucess.
 */
function criarConta($nome, $apelido, $datanasc, $email, $morada, $sexo, $user, $pass) {
	$con = mysql_connect("127.0.0.1", "root", "");
	if ($con == 0) {
		echo 'Failed to connect to MySQL' . "<br />";
	}
	mysql_select_db("contas", $con);
	$op = mysql_query("SELECT email FROM conta WHERE email=\"" . $email . "\"");
	$row = mysql_fetch_row($op);
	if ($row[0] != $email) {
		$row = mysql_query("SELECT COUNT(id) FROM conta");
		$num = mysql_fetch_row($row);
		$numindex = ($num[0] + 1);
		$result = mysql_query("INSERT INTO conta VALUES (" . $numindex . ",
				\"" . $nome . "\",
				\"" . $apelido . "\",
				\"" . $datanasc . "\",
				\"" . $email . "\",
				\"" . $morada . "\",
				\"" . $sexo . "\",
				\"" . $user . "\",
				\"" . $pass . "\"
				)");
	} else {
		$result = FALSE;
	}
	mysql_close($con);
	return $result;
}

/**
 * Função que verifica conta de utilizador
 * @param[in] user The username of client.
 * @param[in] pass The password of client.
 * @param[out] result The result of suceful query or not suceful.
 * @return True or False.
 */
function login($user, $pass) {
	$con = mysql_connect("127.0.0.1", "root", "");
	if ($con == 0) {
		echo 'Failed to connect to MySQL' . "<br />";
	}
	mysql_select_db("contas", $con);
	$result = mysql_query("SELECT user, pass FROM conta WHERE user=\"" . $user . "\" AND pass=\"" . $pass . "\"");
	$row = mysql_fetch_row($result);
	if ($row[0] == $user && $row[1] == $pass) {
		$res = TRUE;
	} else {
		$res = FALSE;
	}
	mysql_close($con);
	return $res;
}

function removeGoes($nome) {

	// store all the Nodes IDs of the identified systems
	$array_nodes_ids = array();
	// temp variable to store the sub-graph
	$xmlOUT = "";

	foreach ($xmlGraph->children() as $child) {
		// key parameter -- rewritten
		//        if ($child->getName() == "key") {
		//            echo "Parameter  ->" . $child->attributes() . "<br />";;
		//
		//        }
		// graph
		if ($child -> getName() == $nome) {
			// inside the graph tag
			foreach ($child->children() as $child2) {
				// check for each node
				if ($child2 -> getName() == "node") {
					// searching on the nodes
					foreach ($child2->children() as $child3) {
						$xmlOUT . $child3 -> attributes() . 'tem o valor' . $child3 . "<br />";
					}
					// for the edges
				} else if ($child2 -> getName() == "edge") {
					echo "Edge element ->  source node= " . $child2 -> attributes() -> source . ", target node= " . $child2 -> attributes() -> target . "<br />";
				}
			}
		}
	}

}

function pesquisaGoES($nome) {
	$ficheiro = simplexml_load_file("Final.graphml");
	//ficheiro a consultar

}

// Use the request to (try to) invoke the service -  XAMMP !!!!
if (!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');
$server -> service($HTTP_RAW_POST_DATA);
?>