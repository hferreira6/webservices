<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title>Apresentação de Interferncias</title>
		</head>
		<body>
			<form action="apresenta.php" method="GET" >
				<br />
				<strong><b> Interferências de um sistema: </b></strong>
				<br />
				<select name="sistema" >
					<option value="XBOX">XBOX</option>
					<option value="TELEVISION">TELEVISION</option>
					<option value="PHONE">PHONE</option>
				</select>
				<br />
				<b>Num intervalo de datas: </b>
				<br />
				Data de inicio:
				<input type="" name="dti" value="01-01-2012" />
				Data de fim:
				<input type="" name="dtf" value="<?php echo date("d-m-Y"); ?>" />
				<br />
				<input type="submit" name="cksis" value="OK" />
			</form>
			<?php
			// Pull in the NuSOAP code
			require_once "../lib/nusoap.php";
			// Create the client instance
			$client = new nusoap_client("http://localhost/Prog/Php/project/WS.php");
			// Check for an error
			$err = $client -> getError();
			if ($err) {
				// Display the error
				echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
				// At this point, you know the call that follows will fail
			} else {
				if (!empty($_GET['sistema']) && !empty($_GET['cksis'])) {
					// Call the SOAP method - don't call both at the same time !
					$result = $client -> call('consultaInt', array('sistema' => $_GET["sistema"], 'firstdaterange' => $_GET["dti"], 'seconddaterange' => $_GET["dtf"]));
				}
			}
			if (!empty($result)) {
				// Check for a fault
				if ($client -> fault) {
					echo '<h2>Fault</h2><pre>';
					print_r($result);
					echo '</pre>';
				} else {
					// Check for errors
					$err = $client -> getError();
					if ($err) {
						// Display the error
						echo '<h2>Error</h2><pre>' . $err . '</pre>';
					} else {
						// Display the result
						if ($result) {
							echo '<h2>WS3 - Interferencias</h2>';
							$result = json_decode($result);
							echo '<table border="1">
							 <tr>
							 <td><b>SYSTEM</b></td>
							 <td><b>CAUSED BY</b></td>
							 <td><b>DATE</b></td>
							 <td><b>SOLVED</b></td>
							 </tr>';
							foreach ($result->estados as $key) {
								echo '<tr>';
								echo '<td>' . $key -> SYSTEM . '</td>';
								echo '<td>' . $key -> CAUSED_BY . '</td>';
								echo '<td>' . $key -> DATE . '</td>';
								echo '<td>' . $key -> SOLVED . '</td>';
								echo '</tr>';
							}
							echo '</table>';
						} else {
							echo '<h2>Erro na consulta de interferencias!</h2>';
						}
					}
				}
				// Display the request and response
				echo '<h2>Request</h2>';
				echo '<pre>' . htmlspecialchars($client -> request, ENT_QUOTES) . '</pre>';
				echo '<h2>Response</h2>';
				echo '<pre>' . htmlspecialchars($client -> response, ENT_QUOTES) . '</pre>';
				// Display the debug messages
				echo '<h2>Debug</h2>';
				echo '<pre>' . htmlspecialchars($client -> debug_str, ENT_QUOTES) . '</pre>';
			}
			?>
		</body>
	</html>
