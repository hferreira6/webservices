<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			</meta>
			<title>Sistemas presentes na Casa</title>
		</head>
		<body>
			<strong><b> Sistemas presentes no Cots </b></strong>
			<table border="2">
				<tr>
					<td align="center" WIDTH="200"><b> Xbox </b></td>
					<td align="center" WIDTH="200"><b> Television </b></td>
					<td align="center" WIDTH="200"><b> Phone </b></td>
				</tr>
				<tr>
					<td align="right" width="200">
					<form  action = "sistemaCots.php" method = "get">
						Action:
						<select name="XBOX" align="right" >
							<option value="1">Start</option>
							<option value="0">Finish</option>
						</select>
						<input align="right" type="submit" name="xbox_button" value="Inserir"/>
					</form></td>
					<td align="right" width="200">
					<form  action = "sistemaCots.php" method = "get">
						Watch:
						<select name="TELEVISION_W">
							<option value="1">1</option>
							<option value="0">0</option>
						</select>
						<input align="right" type="submit" name="tv_button_w" value="Inserir"/>
						<br />
						Power:
						<select name="TELEVISION_P">
							<option value="1">1</option>
							<option value="0">0</option>
						</select>
						<input align="right" type="submit" name="tv_button_p" value="Inserir"/>
					</form></td>
					<td align="right" width="200">
					<form  action = "sistemaCots.php" method = "get">
						Call in:
						<select name="PHONE_CI" align="right">
							<option value="1">1</option>
							<option value="0">0</option>
						</select>
						<input align="right" type="submit" name="phone_button_ci" value="Inserir"/>
						<br />
						Call:
						<select name="PHONE_C" align="right">
							<option value="1">1</option>
							<option value="0">0</option>
						</select>
						<input align="right" type="submit" name="phone_button_c" value="Inserir"/>
						<br />
						Ringing:
						<select name="RINGING" align="right">
							<option value="1">1</option>
							<option value="0">0</option>
						</select>
						<input align="right" type="submit" name="phone_button_r" value="Inserir"/>
					</form></td>
				</tr>
			</table>
			<form  action="apresenta.php" method="get">
				<input align="left" type="submit" name="vi" value="Visualizar Interferencias"/>
			</form>
			<?php
			// Pull in the NuSOAP code
			require_once "../lib/nusoap.php";
			// Create the client instance
			$client = new nusoap_client("http://localhost/Prog/Php/project/WS.php");
			// Check for an error
			$err = $client -> getError();
			if ($err) {
				// Display the error
				echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
				// At this point, you know the call that follows will fail
			}
			if (!empty($_GET['xbox_button'])) {
				$element = 9;
				$feature = 1;
				$type = 'IN';
				$value = $_GET['XBOX'];
			}
			if (!empty($_GET['tv_button_w']) || !empty($_GET['tv_button_p'])) {
				$element = 12;
				if (!empty($_GET['tv_button_w'])) {
					$value = $_GET['TELEVISION_W'];
					$type = 'IN';
					$feature = 48;
				}
				if (!empty($_GET['tv_button_p'])) {
					$value = $_GET['TELEVISION_P'];
					$type = 'IN';
					$feature = 44;
				}
			}
			if (!empty($_GET['phone_button_ci']) || !empty($_GET['phone_button_c']) || !empty($_GET['phone_button_r'])) {
				$element = 2;
				if (!empty($_GET['phone_button_ci'])) {
					$value = $_GET['PHONE_CI'];
					$type = 'OUT';
					$feature = 11;
				}
				if (!empty($_GET['phone_button_c'])) {
					$value = $_GET['PHONE_C'];
					$type = 'IN';
					$feature = 10;
				}
				if (!empty($_GET['phone_button_r'])) {
					$value = $_GET['RINGING'];
					$type = 'IN';
					$feature = 34;
				}
			}
			if (!empty($_GET['xbox_button']) || !empty($_GET['tv_button_w']) || !empty($_GET['tv_button_p']) || !empty($_GET['phone_button_ci']) || !empty($_GET['phone_button_c']) || !empty($_GET['phone_button_r'])) {
				// Call the SOAP method - don't call both at the same time !
				$result = $client -> call('insereEstado', array('element' => $element, 'feature' => $feature, 'value' => $value, 'type' => $type));
			}
			if (!empty($result)) {
				// Check for a fault
				if ($client -> fault) {
					echo '<h2>Fault</h2><pre>';
					print_r($result);
					echo '</pre>';
				} else {
					// Check for errors
					$err = $client -> getError();
				}
				if ($err) {
					// Display the error
					echo '<h2>Error</h2><pre>' . $err . '</pre>';
				} else {
					// Display the result
					if ($result) {
						echo '<h2>Inserido com sucesso.</h2>';
						/**
						 * Chamada a funÃ§Ã£o que consulta o estado dos sistemas Cots, returna para a variavel "estado"
						 * @param[in] xbox The id of system xbox.
						 * @param[in] tv The id of system television.
						 * @param[in] phone The id of system phone.
						 * @param[out] result The result of query.
						 * @return Table of contents of sistems xbox, tv and phone.
						 */
						$estado = $client -> call('consultaEstado', array('xbox' => "9", 'tv' => "12", 'phone' => "2"));
						if ($estado) {
							echo '<h2>WS2 - Estados</h2>';
							$res = json_decode($estado);
							echo '<table border="1">
<tr>
<td><b>ID_SYSTEM</b></td>
<td><b>ID_FEATURE</b></td>
<td><b>VALUE</b></td>
<td><b>DATE</b></td>
<td><b>ID_SOURCE</b></td>
<td><b>IN/OUT</b></td>
</tr>';
							foreach ($res->estados as $key) {
								echo '<tr>';
								echo '<td>' . $key -> ID_SYSTEM . '</td>';
								echo '<td>' . $key -> ID_FEATURE . '</td>';
								echo '<td>' . $key -> VALUE . '</td>';
								echo '<td>' . $key -> DATE . '</td>';
								echo '<td>' . $key -> ID_SOURCE . '</td>';
								echo '<td>' . $key -> INOUT . '</td>';
								echo '</tr>';
							}
							echo '</table>';
						} else {
							echo '<h2>Erro!</h2>';
						}
					}
				}
				// Display the request and response
				echo '<h2>Request</h2>';
				echo '<pre>' . htmlspecialchars($client -> request, ENT_QUOTES) . '</pre>';
				echo '<h2>Response</h2>';
				echo '<pre>' . htmlspecialchars($client -> response, ENT_QUOTES) . '</pre>';
				// Display the debug messages
				echo '<h2>Debug</h2>';
				echo '<pre>' . htmlspecialchars($client -> debug_str, ENT_QUOTES) . '</pre>';
			}
			?>
		</body>
	</html>
