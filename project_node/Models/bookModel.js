/* 
E a classe Book, por JSON.
Esta classe será usada pelo mongo para elaborar a BD
*/ 
var mongoose= require('mongoose'),
    Schema=mongoose.Schema;

var bookModel = new Schema({
    title:{type:String},
    author:{type:String},
    genre:{type:String},
    read:{type:Boolean, default:false}
});


//adiciona um modulo ao mongos Bd
module.exports=mongoose.model('Book',bookModel);
