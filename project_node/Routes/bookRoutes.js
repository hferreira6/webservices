/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var express =require('express'),
    mongoose =require('mongoose');

var bodyParser=require('body-parser');    


var routes= function(Book){
    var bookRouter=express.Router();

    bookRouter.route('/')
        .post(function(req,res){
            console.log("entrei no post!!");
            console.log("o book!e "+req.body);
            var book= new Book(req.body);  
          // var book=new Book({"title": "War and Peace","author": "Lev Nikolayevich Tostoy", "genre" : "Historical Fiction"});
          // var book=new Book({"title": "My new Book" , "author": "Ferreira" , "genre" : "Fiction"});
            book.save(); //Grava o book na bD
            res.status(201).send(book);    
         }) // sem ; pois o metodo get vem do BookRoute 
       
        .get(function(req,res){
           console.log("entrei no get!!");
               //inicia um filtro
           var query={};
           if(req.query.genre){ // verifica se o que está depois do ? existe na bd como campo
               query.genre=req.query.genre;
           }
           //fim de filtro
           Book.find(query, function(err,books){
            if(err){
                res.status(500).send(err);  
                console.log("erro é "+err+" this is my api/book");
            }
            else{
                
                res.json(books);
            }
            });
    });
    
    
    
    
    // Este codigo é criado como Midelware pois ele é repetido varias vezes
    bookRouter.use('/:bookId', function(req,res,next){
        Book.findById(req.params.bookId, function(err,book){
            if(err)
                //res.status(500).send(err);  
                console.log("erro é "+err+" this is my api/book");
            
            else if(book){
                console.log("estou a pesquisar o livro");
                req.book=book;
                next();
            }
            else{
                res.status(404).send("not found!!!");
            }
        });
    });

    bookRouter.route('/:bookId')
        .get(function(req,res){
            res.json(req.book);   
        })
  
        .put(function(req,res){
            console.log("o book!e "+req.book.title);
            req.book.title=req.body.title;
            req.book.author=req.body.author;
            req.book.genre=req.body.genre;
            req.book.save();
            res.json(req.book);
        })
         //PATCH faz um update de só um campo
       .patch(function(req,res){
           if(req.body._id){ 
               delete req.body._id;
            }        
            for(var p in req.body){
                req.book[p]=req.body[p];
            }
            req.book.save();
            res.json(req.book);
            console.log(" o book a salvar é "+req.book);
        })
        
       .delete(function(req,res){
           console.log("entrei no delete");
            req.book.remove(function(err){
               if(err) res.status(500).send("erro: "+err);
               else{
                   res.status(204).send("Removed");
               }
            });
        });
    
    return bookRouter;
    
};
module.exports=routes;
