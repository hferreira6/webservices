/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global process, router, bodyParser, book */
var express =require('express'),
        mongoose =require('mongoose');
var bodyParser=require('body-parser');    

//    postgres=require('db.js')
//var db=postgres();
var db =mongoose.connect('mongodb://localhost/bookAPI');

//iram ser as classes
var Book =require('./Models/bookModel');

// var app é a aplicacao
var app=express();   
var port =process.env.PORT || 3000;

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

bookRouter=require('./Routes/bookRoutes')(Book); // chamada 

app.use('/api/Books',bookRouter); // vai buscar tdas as routes

app.get('/',function(req,res){
    var responsejson={hello:"this is my API"};
    res.json(responsejson);
});

app.listen(port,function(){
    console.log('Running Port: '+port);
});
