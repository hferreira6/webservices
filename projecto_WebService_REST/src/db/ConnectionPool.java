/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

//import org.apache.log4j.Logger;
import java.sql.*;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A class for preallocating, recycling, and managing JDBC connections.
 * <P>
 * Taken from Core Servlets and JavaServer Pages from Prentice Hall and Sun
 * Microsystems Press, http://www.coreservlets.com/. &copy; 2000 Marty Hall; may
 * be freely used or adapted.
 *
 * Adaptada para a disciplina de Bases de Dados, UFP, 2012
 *
 */
public class ConnectionPool {
	private static String driver = "org.postgresql.Driver";
	private static String url = "jdbc:postgresql://127.0.0.1:5432/CET?charSet=UTF8";
	private static String username = "hferreira";
	private static String password = "postgres";
	private static int maxConnections = 2;
	private static Vector<Connection> availableConnections, busyConnections;
	private static boolean initialized = false;

	// private static Logger log = Logger.getLogger(ConnectionPool.class);

	private static synchronized void init() throws SQLException {
		if (initialized)
			return;
		availableConnections = new Vector<Connection>(maxConnections);
		busyConnections = new Vector<Connection>();
		for (int i = 0; i < maxConnections; i++) {
			availableConnections.addElement(makeNewConnection());
		}
		initialized = true;
	}

	public static synchronized Connection getConnection() throws SQLException {
		init();
		if (!availableConnections.isEmpty()) {
			Connection existingConnection = availableConnections
					.lastElement();
			int lastIndex = availableConnections.size() - 1;
			availableConnections.removeElementAt(lastIndex);
			if (existingConnection.isClosed()) {
				// log.error("No more connections!!");
				System.out.println("No more connections!!");
				throw new SQLException("Connection is closed");
			} else {
				busyConnections.addElement(existingConnection);
				return (existingConnection);
			}
		} else {
			System.out.println("No more connections!!");
			// log.error("No more connections");
			throw new SQLException("Connection limit reached");
		}
	}

	private static Connection makeNewConnection() throws SQLException {
		try {
			// Load database driver if not already loaded
			Class.forName(driver);
			// Establish network connection to database
			Connection connection = DriverManager.getConnection(url, username,
					password);
			return (connection);
		} catch (ClassNotFoundException cnfe) {
			// Simplify try/catch blocks of people using this by
			// throwing only one exception type.
			throw new SQLException("Can't find class for driver: " + driver);
		}
	}

	public static synchronized void free(Connection connection) {
		busyConnections.removeElement(connection);
		availableConnections.addElement(connection);
	}

	public static void close(PreparedStatement st) {
		if (st != null)
			try {
				st.close();
			} catch (SQLException ex) {
				Logger.getLogger(ConnectionPool.class.getName()).log(
						Level.SEVERE, null, ex);
			}
	}

	public static void close(Statement st) {
		if (st != null)
			try {
				st.close();
			} catch (SQLException ex) {
				Logger.getLogger(ConnectionPool.class.getName()).log(
						Level.SEVERE, null, ex);
			}
	}

	public static void close(ResultSet rs) {
		if (rs != null)
			try {
				rs.close();
			} catch (SQLException ex) {
				Logger.getLogger(ConnectionPool.class.getName()).log(
						Level.SEVERE, null, ex);
			}
	}

	public static synchronized int totalConnections() {
		return (availableConnections.size() + busyConnections.size());
	}

	/**
	 * Close all the connections. Use with caution: be sure no connections are
	 * in use before calling. Note that you are not <I>required</I> to call this
	 * when done with a ConnectionPool, since connections are guaranteed to be
	 * closed when garbage collected. But this method gives more control
	 * regarding when the connections are closed.
	 */
	public static synchronized void closeAllConnections() {
		closeConnections(availableConnections);
		availableConnections = new Vector<Connection>();
		closeConnections(busyConnections);
		busyConnections = new Vector<Connection>();
	}

	private static void closeConnections(Vector<Connection> connections) {
		try {
			for (int i = 0; i < connections.size(); i++) {
				Connection connection = connections.elementAt(i);
				if (!connection.isClosed()) {
					connection.close();
				}
			}
		} catch (SQLException sqle) {
			// Ignore errors; garbage collect anyhow
			// log.error("Exception is "+sqle);
		}
	}
}
