/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ufp.app;



import java.io.IOException;




import java.util.ArrayList;







//import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ufp.Course;
import edu.ufp.Student;
import edu.ufp.service.CourseService;

/**
 *
 * @author Hugo Ferreira
 * Este Servlet faz o encaminhamento  na area dos cursos
 */
@WebServlet(name = "Servlet", urlPatterns = {"/servlet/*"})
public class CourseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
    public CourseServlet() {
    	super();     
    	System.out.println("*****Servlet******");
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	System.out.println("****GET*Course*****");
    	String param[] = parseURL(request);
    	if(param!=null){
		if (param[1].equals("list")) {
			ArrayList<Student> st=CourseService.getStudentInClass(101);
			request.setAttribute("student", st);
			request.getRequestDispatcher("/WEB-INF/jsp/course/courselist.jsp").forward(request, response);
		}
		else if(param[1].equals("new")) {
			request.getRequestDispatcher("/WEB-INF/jsp/course/coursenew.jsp").forward(request, response);
		}}
    	else {	ArrayList<Course> cs=CourseService.getAllCourses();
		request.setAttribute("course", cs);
		request.getRequestDispatcher("/WEB-INF/jsp/courses.jsp").forward(request, response);
    	}
	
		
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	System.out.println("**********POST*Course***********");//cria  nova  discipllina
		int id = Integer.parseInt(request.getParameter("name"));
		String disp = request.getParameter("surname");
		System.out.println("name: " + disp + " id: " + id);
		CourseService.createCourse(disp.toUpperCase(), id);
	    request.getRequestDispatcher("/WEB-INF/jsp/courses.jsp").forward(request, response);
		
	}
	/*
	 * Utility class
	 */
	public static String[] parseURL(HttpServletRequest req) {
		String parameter = req.getPathInfo();
		if ((parameter == null) || (parameter.equals("/*")))
			return null;
		else {
			System.out.println(parameter.toString());
			return parameter.split("/");
		}
	}


}
