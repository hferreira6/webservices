/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ufp.app;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
//import java.sql.Date;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ufp.service.AuteticationService;
import db.ConnectionPool;

/**
 * 
 * @author Beatriz
 * alterado por Hugo em dia 9/12/2014
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login/*"})
public class LoginServlet extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	System.out.println("LoginServlet.processRequest()");
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        	System.out.println("aquis");
             if (request.getSession().getAttribute("login") == null) {
            	  	System.out.println("aquis II");
                request.getSession().setAttribute("login", "login"); // passa a string "login" que se chama login 
            } else {
                request.getSession().removeAttribute("login"); // retira o atributo login
            }

            request.getRequestDispatcher("/WEB-INF/jsp/students.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	System.out.println("**********GET login************");
		String param[] = parseURL(request);
		if (param == null) {
		 	request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
		}
		else if (param[1].equals("register")) {
			request.getRequestDispatcher("/WEB-INF/jsp/login/register.jsp").forward(request, response);
		}
   
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       // processRequest(request, response);
    	System.out.println("**********POST login************");
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		System.out.println("USERNAME: " + userName);
		System.out.println("PASSWORD: " + password);
		AuteticationService auth=new AuteticationService();
		try {
			// verifica se o "userName" já existe na DB é true
			if(auth.authenticate(ConnectionPool.getConnection(), userName, password)==false){
				System.out.println("entrei para criar login ");
				boolean user=auth.createUser(ConnectionPool.getConnection(), userName, password);
				if(user==true) doPost(request, response);
			}
			else{
				// give a session
				HttpSession sess = request.getSession();
				System.out.println("IN VALIDATESERV DOPOST "+sess);

				  request.setAttribute("login", userName);
				  request.getRequestDispatcher("/WEB-INF/jsp/menu.jsp").forward(request, response);
			}
		} catch (NoSuchAlgorithmException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
  }

    /**
     * Returns a URL parameter.
	 * Utility class
	 */
	public static String[] parseURL(HttpServletRequest req) {
		String parameter = req.getPathInfo();
		if ((parameter == null) || (parameter.equals("/*")))
			return null;
		else {
			System.out.println(parameter.toString());
			return parameter.split("/");
		}
	}

}
