package edu.ufp.app;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ufp.Course;
import edu.ufp.Student;
import edu.ufp.service.CourseService;
import edu.ufp.service.StudentService;

@WebServlet(name = "StudentServlet", urlPatterns = { "/student/*" })
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private boolean flag = false;
	int id;

	/**
	 * @see HttpServlet#HttpServlet() - iniciar a Servelt
	 */
	public StudentServlet() {
		super(); // TODO Auto-generated constructor stub
		System.out.println("**********StudentServlet************");

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		System.out.println("**********GET************");
		String param[] = parseURL(request);
		if (param == null) {
			request.getRequestDispatcher("/WEB-INF/jsp/students.jsp").forward(
					request, response);
		}
		else if (param.length > 1) {
					if (param[1].equals("security")) {
						if (param[2].equals("new")){ 
							request.getRequestDispatcher("/WEB-INF/jsp/student/studentnew.jsp").forward(request,response);
					    }

						else if (param[2].equals("course")){
							System.out.println("**********course************");
							ArrayList<Course> cs=CourseService.getAllCourses();
							request.setAttribute("course", cs);
							request.getRequestDispatcher("/WEB-INF/jsp/courses.jsp").forward(request, response);
				        }else {
					    	System.out.println("**********search************");
					    	String name = request.getParameter("name");
					    	String id_aux=request.getParameter("id");
					    	System.out.println("name: "+name+" id:"+id_aux);    	
						if(name!=""){
							ArrayList<Student> st = StudentService.getStudentByName(name);
							request.setAttribute("student", st);
							System.out.println("array: " + st.size());
							request.getRequestDispatcher("/WEB-INF/jsp/student/studentlist.jsp").forward(request, response);
						}else{
						// By ID
							id = Integer.parseInt(request.getParameter("id"));
							Student st=StudentService.getStudentById(id);
							System.out.println("In Search-Student: "+st.getApelido());
							
							request.setAttribute("student", st);
							request.getRequestDispatcher("/WEB-INF/jsp/student/studentedit.jsp").forward(request, response);
						}
							
					    }
				}else if (param[1].equals("list")){
					System.out.println("**********list************");
					ArrayList<Student> st = StudentService.getAllStudents();
					request.setAttribute("student", st); // envia o array de st com o nome "student"
					request.getRequestDispatcher("/WEB-INF/jsp/student/studentlist.jsp").forward(request, response);
				}
				else{
					System.out.println("**********edit************");
					int cid = Integer.parseInt(param[1]);
					System.out.println("Id "+cid);
					Student student = StudentService.getStudentById(cid);			
					request.setAttribute("student", student);
					String control=request.getParameter("edit");
					if(control!=null){if(control.equalsIgnoreCase("Editar"))
					request.getRequestDispatcher("/WEB-INF/jsp/student/studentedit.jsp").forward(request, response);	
					else if(control.equalsIgnoreCase("Adicionar_disciplina")){
						ArrayList<Course> cs=CourseService.getAllCourses();
						request.setAttribute("course", cs);
						request.getRequestDispatcher("/WEB-INF/jsp/student/student_course.jsp").forward(request, response);
						
					}}
					else request.getRequestDispatcher("/WEB-INF/jsp/student/studentedit.jsp").forward(request, response);
			}		
		}
	}


	
	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request
	 * 
	 *Este Metodo é chamado pelo browser quando se pretende criar um novo student
	 *tambem é chamado quando se pretende edital ao eliminar um student
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		System.out.println("**********POST************");
		String override = request.getParameter("_method");
		System.out.println("override " + override);
		if (override == null) {
			String name = request.getParameter("name");
			String aplido = request.getParameter("surname");
			System.out.println("name: " + name + " surname: " + aplido);
			if (name != "" && aplido != "") {
				StudentService.createStudent(aplido, name);
				request.getRequestDispatcher("/WEB-INF/jsp/students.jsp").forward(request, response);
			}
		} else {
			if (override.equals("Delete")||override.equals("Remove")) {
				flag=false; // para garantir que a flag quando vem para o remover é falsa
				if(override.equals("Remove"))flag=true;  // flag a true retirar um aluno da disciplina a tabela turma
				doDelete(request, response);
				return;
			} else if (override.equals("Editar")||override.equals("Adicionar_disciplina")) {
				flag=false; // para garantir que a flag quando vem para o editar é falsa
				if(override.equals("Adicionar_disciplina"))flag=true;  // flag a true para no put entrar naa funçao de adicionar a tabela turma
				doPut(request, response);
				return;
			}
		}
	}

	@Override
	protected void doPut(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		System.out.println("**********PUT************");
		System.out.println("PUT flag " + flag);
		if (flag == false) {
			int id = Integer.parseInt(request.getParameter("id"));
			System.out.println("ID "+id);
			String nome = request.getParameter("name");
			String apelido = request.getParameter("surname");
			System.out.println("Edit nome: "+nome+ " Aplido: "+apelido);
			StudentService.editStudent(nome, apelido, id);
		} else { //adiciona uma disciplina num aluno vai para o servelt  
			flag = false;
			int id = Integer.parseInt(request.getParameter("id"));
			int disciplina_id=Integer.parseInt(request.getParameter("disciplina"));
			System.out.println("Edit class id: " + id + " nome disciplina: "+disciplina_id );
			StudentService.editStudent(disciplina_id, id); // vai adicionar o aluno a uma disciplina
		}
		request.getRequestDispatcher("/WEB-INF/jsp/students.jsp").forward(request, response);
	}

	@Override
	protected void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		System.out.println("**********DELETE************");
		System.out.println("PUT flag " + flag);
		if(flag == false){
				int id = Integer.parseInt(request.getParameter("student"));
				System.out.println("id: " + id);
				StudentService.deleteStudent(id);
				request.getRequestDispatcher("/WEB-INF/jsp/students.jsp").forward(request,response);
		} else {  
			flag = false;		
			int id = Integer.parseInt(request.getParameter("student"));
			System.out.println("id: " + id);
			CourseService.removeStudent(id);
			request.getRequestDispatcher("/WEB-INF/jsp/menu.jsp").forward(request,response);
		}// criar aqui uma condiçao com o Parametro de name onde=curso/aluno
	}

	/*
	 * Utility class
	 */
	public static String[] parseURL(HttpServletRequest req) {
		String parameter = req.getPathInfo();
		if ((parameter == null) || (parameter.equals("/*")))
			return null;
		else {
			System.out.println(parameter.toString());
			return parameter.split("/");
		}
	}

}