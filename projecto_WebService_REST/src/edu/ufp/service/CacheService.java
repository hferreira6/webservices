package edu.ufp.service;




//import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
//import net.sf.ehcache.Statistics;

public class CacheService {

	private static CacheManager cacheManager = CacheManager.create();
	
	public void init(){
		System.out.println("Cache Manager init");
		cacheManager.addCache("myCache");
		System.out.println("Created ehcache");
	}

	/***
	*------------------------------------------------------------------------------------------
	*
	* EHCache code.
	*
	*------------------------------------------------------------------------------------------
	***/
	public synchronized void putCache (String key, Object value){
		putCache(key, value, "myCache");
	}
	public synchronized void putCache (String key, Object value, String cacheId){
		Element element = new Element(key, value);
		Cache cache = cacheManager.getCache(cacheId);
		cache.put(element);
	}
	public synchronized void removeFromCache (String key){
		removeFromCache(key, "myCache");
	}
	public synchronized void removeFromCache (String key, String cacheId){
		Cache cache = cacheManager.getCache(cacheId);
		cache.remove(key);
	}
	public synchronized Object getFromCache (String key){
		return getFromCache(key, "myCache");
	}
	public synchronized Object getFromCache (String key, String cacheId){
		Cache cache = cacheManager.getCache(cacheId);
		Element element = cache.get(key);
		if(element == null)
			return null;
		else
			return element.getObjectValue();
	}
}