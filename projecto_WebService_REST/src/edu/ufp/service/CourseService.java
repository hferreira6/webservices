package edu.ufp.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.ufp.Course;
import edu.ufp.Student;
import db.Client;
import db.ConnectionPool;

public class CourseService {

	public static void createCourse(String disp, int id) {

    	System.out.println("Nome: "+disp+" Aplido: "+id);
        String query="insert into disciplinas (disciplina, id) values ('"+disp+"',"+id+");";
        Client cl=new Client();
        cl.client(query);
	}
    
  /**
   * A FUNCIONAR
   * Metodo que premite criar um Array com todos os Course(s)
   */
       @SuppressWarnings("finally")
	public static ArrayList<Course> getAllCourses() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Course> course = new ArrayList<Course>();
        try {
            conn = ConnectionPool.getConnection();
            pstmt = conn.prepareStatement("select * from disciplinas");
            rs = pstmt.executeQuery();

            while(rs.next()){
             Course course_aux = new Course();
             course_aux.setId(Integer.parseInt(rs.getString(2)));
             course_aux.setDisciplna(rs.getString(1));
             course.add(course_aux); 
           }
        } catch (SQLException ex) { 
            Logger.getLogger(StudentService.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            if(conn != null)
            ConnectionPool.free(conn);
            ConnectionPool.close(pstmt);
            ConnectionPool.close(rs);
            return course;
        }
        
    }

       /**
        * A FUNCIONAR
        * Metodo que premite criar um Array de alunos de um Course especifico)
        */
            @SuppressWarnings("finally")
     	public static ArrayList<Student> getStudentInClass(int id_disciplina) {
             Connection conn = null;
             PreparedStatement pstmt = null;
             ResultSet rs = null;
             ArrayList<Student> course = new ArrayList<Student>();
             try {
                 conn = ConnectionPool.getConnection();
                 pstmt = conn.prepareStatement("select a.nome, a.aplido, a.id from aluno a where a.id IN(select t.id_aluno from turma t where t.id_disciplina="+id_disciplina+");");
                 rs = pstmt.executeQuery();

                 while(rs.next()){
                  Student student= new Student();
                  System.out.print("ID: "+rs.getString(3));
                  student.setNome(rs.getString(1));
                  student.setApelido(rs.getString(2));
                  student.setId(Integer.parseInt(rs.getString(3)));
                  course.add(student); 
                }
             } catch (SQLException ex) { 
                 Logger.getLogger(StudentService.class.getName()).log(Level.SEVERE, null, ex);
             } finally{
                 if(conn != null)
                 ConnectionPool.free(conn);
                 ConnectionPool.close(pstmt);
                 ConnectionPool.close(rs);
                 System.out.println("CourseService.getStudentInClass()"+course.size());
                 return course;
             }
             
         }
            /**
             * A FUNCIONAR
             * Metodo que remove aluno de um Course especifico
             */
            public static void removeStudent(int id) {
            	System.out.println("CourseService.removeStudent()");
                String query="DELETE FROM turma * WHERE id_aluno="+id+";";
                Client cl=new Client();
                cl.client(query);
            
        }
}