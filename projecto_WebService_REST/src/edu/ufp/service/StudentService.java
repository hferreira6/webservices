package edu.ufp.service;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import edu.ufp.Student;
import db.Client;
import db.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Beatriz
 * alterado por Hugo Ferreira
 */
public class StudentService {

    public static ArrayList<Student> st = new ArrayList<Student>();
    
    /**
     * A FUNCIONAR
     * Metodo que vai buscar todos os campos de um id, constroi o Student
     **/
    @SuppressWarnings("finally")
	public static Student getStudentById(int id) {
        Connection conn = null; 
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Student student = new Student();
        try {
            conn = ConnectionPool.getConnection();
            pstmt = conn.prepareStatement("select * from aluno where id=? ");
            pstmt.setLong(1, id);	
            System.out.println("query: "+pstmt);
            rs = pstmt.executeQuery();
            if(rs.next()){
                System.out.println("In getStudentById-nome: "+rs.getString(2));
                student.setNome(rs.getString(2));
                student.setApelido(rs.getString(3));
                student.setId(id);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentService.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            if(conn != null)
            ConnectionPool.free(conn);
            ConnectionPool.close(pstmt);
            ConnectionPool.close(rs);
            return student;
        }
    }
    

	@SuppressWarnings("finally")
	public static ArrayList<Student> getStudentByName(String name) {
	        Connection conn = null; 
	        PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        ArrayList<Student> st=new ArrayList<Student>();
	        
	        try {
	            conn = ConnectionPool.getConnection();
	            pstmt = conn.prepareStatement("select * from aluno where nome=?");
	            pstmt.setString(1, name);// o 1 representa o primerio "?" na query
	            rs = pstmt.executeQuery();
	            while(rs.next()){
	            	Student student = new Student();
	                System.out.println("In getStudentByName-nome: "+rs.getString(2));
	                student.setNome(rs.getString(2));
	                student.setApelido(rs.getString(3));
	                student.setId(rs.getInt(1));
	                st.add(student);
	            }
	        } catch (SQLException ex) {
	            Logger.getLogger(StudentService.class.getName()).log(Level.SEVERE, null, ex);
	        } finally{
	            if(conn != null)
	            ConnectionPool.free(conn);
	            ConnectionPool.close(pstmt);
	            ConnectionPool.close(rs);
	            return st;
	        }
	    }
	
    
  /**
   * A FUNCIONAR
   * Metodo que premite criar um Array com todos os Student(s)
   */
       @SuppressWarnings("finally")
	public static ArrayList<Student> getAllStudents() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Student> students = new ArrayList<Student>();
        try {
            conn = ConnectionPool.getConnection();
            pstmt = conn.prepareStatement("select * from aluno");
            rs = pstmt.executeQuery();

            while(rs.next()){
             Student student = new Student();
             student.setId(Integer.parseInt(rs.getString(1)));
             student.setNome(rs.getString(2));
             student.setApelido(rs.getString(3));
             students.add(student); 
           }
        } catch (SQLException ex) { 
            Logger.getLogger(StudentService.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            if(conn != null)
            ConnectionPool.free(conn);
            ConnectionPool.close(pstmt);
            ConnectionPool.close(rs);
            return students;
        }
        
    }

    /**
     * A FUNCIONAR
     * Metodo que premite inserir o aluno Student
     */       
    public static void createStudent(String apelido, String nome) {

    	System.out.println("Nome: "+nome+" Aplido: "+apelido);
        String query="insert into aluno (nome, aplido) values ('"+nome+"','"+apelido+"');";
        Client cl=new Client();
        cl.client(query);
    }
    
    /** 
     *   FUNCIONAR
     * Metodo que premite editar um Student
     * 
     */
	public static void editStudent(String name, String surname, int id) {
		String str_id=Integer.toString(id);
        String query="update aluno set nome= '"+name+"', aplido='"+surname+"' where id="+str_id+";";   
        //String query="update aluno set nome='"+name+"' where id=32;";

        Client cl=new Client();
        cl.client(query);
	}
         

    /**
     * A FUNCIONAR
     * Metodo que premite eliminar o aluno Student
     */
    public static void deleteStudent(int id) {
            String query="DELETE FROM aluno * WHERE id="+id+";";
            Client cl=new Client();
            cl.client(query);
        
    }

    /**
     * A FUNCIONAR
     * Adiciona o aluno a disciplina, criando nova linha na tabela "turma"
     * @param disciplina_id chave 
     * @param id
     */
	public static void editStudent(int disciplina, int student) {
        String query="insert into turma (id_aluno, id_disciplina) values ("+student+","+disciplina+");";
        Client cl=new Client();
        cl.client(query);
	}





}